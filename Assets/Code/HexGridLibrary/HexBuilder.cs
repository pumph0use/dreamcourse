﻿using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class HexBuilder
{

    #region Variables

    private static float ABSOLUTE_FLOOR;

    //TODO: TEMPORARY UV SOLUTION. Will convert to a "calculated UV map" at a later time.
    public static readonly Vector2[] UV = {
        new Vector2(0,0.25f),
        new Vector2(0,0.75f),
        new Vector2(0.5f,1),
        new Vector2(1,0.75f),
        new Vector2(1,0.25f),
        new Vector2(0.5f,0)
    };

    public static readonly Vector2[] UVWALL =
    {
        new Vector2(0, 0),
        new Vector2(1, 0),
        new Vector2(1, 1),
        new Vector2(0, 1)
    };

    #endregion

    #region Public Methods

    public static void SetAbsoluteFloor(float lowestPoint)
    {
        ABSOLUTE_FLOOR = lowestPoint;
    }

    public static void GenerateHexTop(HexPoint center, float yPos, HexMeshData meshData)
    {
        /*0*/
        meshData.AddVertex(GenerateTopVertex(center, 3));
        /*1*/   
        meshData.AddVertex(GenerateTopVertex(center, 2));
        /*2*/   
        meshData.AddVertex(GenerateTopVertex(center, 1));
        /*3*/   
        meshData.AddVertex(GenerateTopVertex(center, 0));
        /*4*/   
        meshData.AddVertex(GenerateTopVertex(center, 5));
        /*5*/   
        meshData.AddVertex(GenerateTopVertex(center, 4));

        meshData.AddTopTriangles();
        meshData.AddUVRange(UV);
    }

    public static void GenerateWall(HexPoint center, int direction, HexMeshData meshData)
    {
        var core = Map.Instance[center];
        var neighbor = Map.Instance[HexPoint.GetNeighbor(center, direction)];
        var yPos = neighbor == null ? ABSOLUTE_FLOOR : neighbor.YPos;
        Debug.Log(neighbor == null);
        if (WallCheck(core, neighbor))
        {
            switch (direction)
            {
                case 0:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 5), GenerateTopVertex(center, 0), yPos);
                    break;
                case 1:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 0), GenerateTopVertex(center, 1), yPos);
                    break;
                case 2:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 1), GenerateTopVertex(center, 2), yPos);
                    break;
                case 3:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 2), GenerateTopVertex(center, 3), yPos);
                    break;
                case 4:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 3), GenerateTopVertex(center, 4), yPos);
                    break;
                case 5:
                    GenerateWallVertices(meshData, GenerateTopVertex(center, 4), GenerateTopVertex(center, 5), yPos);
                    break;
                default:
                    Debug.LogError(string.Format("Cannot Generate Wall Vertex, Direction must be 0-5. You entered: {0}", direction));
                    break;
            }
        }
    }

    #endregion

    #region Vertex Generation Methods

    private static Vector3 GenerateTopVertex(Vector3 center, int vertexNumber)
    {
        return new Vector3((center.x + Map.Instance.size * Mathf.Cos(2 * Mathf.PI * (vertexNumber + 0.5f) / 6)), center.y,
            (center.z + Map.Instance.size * Mathf.Sin(2 * Mathf.PI * (vertexNumber + 0.5f) / 6)));
    }

    public static void GenerateWallVertices(HexMeshData meshData, Vector3 firstVertex, Vector3 secondVertex, float height)
    {
        meshData.AddVertex(firstVertex);
        meshData.AddVertex(secondVertex);
        meshData.AddVertex(new Vector3(secondVertex.x, height, secondVertex.z));
        meshData.AddVertex(new Vector3(firstVertex.x, height, firstVertex.z));
        meshData.AddWallTriangles();
        meshData.AddUVRange(UVWALL);
    }

    #endregion


    #region HexBuilder Helper Methods

    private static bool WallCheck(IHexTile center, IHexTile neighbor)
    {
        if (neighbor != null && neighbor.YPos < center.YPos)
            return true;
        if (center.YPos > ABSOLUTE_FLOOR)
            return true;
        return false;
    }


    #endregion
}