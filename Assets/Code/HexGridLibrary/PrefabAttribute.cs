﻿using System;

[AttributeUsage(AttributeTargets.Class, Inherited = true)]
public class PrefabAttribute : Attribute
{
    public readonly string name;
    public readonly bool persistent;

    public PrefabAttribute(string name, bool persistent = false)
    {
        this.name = name;
        this.persistent = persistent;
    }
}