﻿public interface IHexTile
{
    float YPos { get; set; }

    void BuildTile(HexMeshData meshData, HexPoint center);
}