﻿using System.Collections.Generic;
using UnityEngine;

public class HexMeshData
{
    public List<Vector3> vertices;
    public List<Vector2> uv;
    public List<int> triangles;

    public HexMeshData()
    {
        vertices = new List<Vector3>();
        uv = new List<Vector2>();
        triangles = new List<int>();
    }

    public void AddVertex(Vector3 vertex)
    {
        vertices.Add(vertex);
    }


    //public void GenerateHexTop(HexPoint center, float yPos, HexMeshData meshData)
    //{
    //    /*0*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 3));
    //    /*1*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 2));
    //    /*2*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 1));
    //    /*3*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 0));
    //    /*4*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 5));
    //    /*5*/
    //    AddVertex(GenerateTopVertex(center.GridToWorldSpace(yPos), 4));

    //    AddTopTriangles();

    //}

    //private Vector3 GenerateTopVertex(Vector3 center, int vertexNumber)
    //{
    //    return new Vector3((center.x + Map.Instance.size * Mathf.Cos(2 * Mathf.PI * (vertexNumber + 0.5f) / 6)), center.y,
    //        (center.z + Map.Instance.size * Mathf.Sin(2 * Mathf.PI * (vertexNumber + 0.5f) / 6)));
    //}


    public void AddTopTriangles()
    {
        var vertexCount = vertices.Count;
        
        //       1,5,0,
        triangles.Add(vertexCount - 5);
        triangles.Add(vertexCount - 1);
        triangles.Add(vertexCount - 6);

        //       1,4,5,
        triangles.Add(vertexCount - 5);
        triangles.Add(vertexCount - 2);
        triangles.Add(vertexCount - 1);
        
        //       1,2,4,
        triangles.Add(vertexCount - 5);
        triangles.Add(vertexCount - 4);
        triangles.Add(vertexCount - 2);

        //       2,3,4
        triangles.Add(vertexCount - 4);
        triangles.Add(vertexCount - 3);
        triangles.Add(vertexCount - 2);
    }

    public void AddWallTriangles()
    {
        var vertexCount = vertices.Count;

        triangles.Add(vertexCount - 4);
        triangles.Add(vertexCount - 2);
        triangles.Add(vertexCount - 1);

        triangles.Add(vertexCount - 4);
        triangles.Add(vertexCount - 3);
        triangles.Add(vertexCount - 2);
    }

    public void AddUVRange(Vector2[] uvArray)
    {
        uv.AddRange(uvArray);
    }
}