﻿using System.Collections.Generic;
using UnityEngine;

[Prefab("Map", true)]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Map : Singleton<Map>
{
    public float size;
    public Texture texture;

    private HexMeshData m_meshData;
    private MeshFilter m_filter;
    private MeshRenderer m_renderer;
    private Mesh m_mesh;
    private Dictionary<HexPoint, IHexTile> m_mapDictionary;
    private List<GameObject> neighbors; 

    public IHexTile this[HexPoint key]
    {
        get
        {
            IHexTile temp = null;
            if (m_mapDictionary.TryGetValue(key, out temp))
            {
                return temp;
            }
            return null;
        }
    }

    public Dictionary<HexPoint, IHexTile> MapGrid
    {
        get
        {
            if(m_mapDictionary == null)
                m_mapDictionary = new Dictionary<HexPoint, IHexTile>();
            return m_mapDictionary;
        }    
    }

    void Start()
    {
        MapReset();
    }

    //void OnDrawGizmos()
    //{
    //    List<HexPoint> temp = new List<HexPoint>();
    //    foreach (var tile in MapGrid)
    //    {
    //        temp.AddRange(HexPoint.GetAllNeighbors(tile.Key));
    //    }
    //    foreach (HexPoint point in temp)
    //    {
    //        Debug.Log(point.ToString());
    //        Mesh m = new Mesh();
    //        m.vertices = HexBuilder.GenerateHexTop(point, size, 0);
    //        m.triangles = HexBuilder.SetTopTriangles(m.vertices.Length);
    //        m.uv = HexBuilder.UV;
    //        m.RecalculateNormals();
    //        Gizmos.DrawMesh(m, point.GridToWorldSpace(size, 0));
    //    }
    //}

    public void MapReset()
    {
        gameObject.transform.position = Vector3.zero;
        m_meshData = new HexMeshData();
        m_mesh = new Mesh();
        m_filter = GetComponent<MeshFilter>();
        m_renderer = GetComponent<MeshRenderer>();

        MapGrid.Clear();
        MapGrid.Add(new HexPoint(0,0), new GrassHexTile(5) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 0), new GrassHexTile(4) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 1), new GrassHexTile(3) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 2), new GrassHexTile(2) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 3), new GrassHexTile(1) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 4), new GrassHexTile(0) );
        MapGrid.Add(HexPoint.GetNeighbor(HexPoint.zero, 5), new GrassHexTile(-1));

        HexBuilder.SetAbsoluteFloor(-2f);

        UpdateMesh();
    }

    public void UpdateMesh()
    {
        foreach (KeyValuePair<HexPoint, IHexTile> pair in m_mapDictionary)
        {
            MapGrid[pair.Key].BuildTile(m_meshData, pair.Key);
        }

        m_mesh.vertices = m_meshData.vertices.ToArray();
        m_mesh.triangles = m_meshData.triangles.ToArray();
        m_mesh.uv = new Vector2[m_meshData.vertices.Count];

        m_mesh.RecalculateNormals();

        m_filter.mesh = m_mesh;

        m_renderer.sharedMaterial.mainTexture = texture;
    }
}