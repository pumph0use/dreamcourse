﻿using System;
using System.Collections.Generic;
using UnityEngine;

public struct HexPoint
{
    public override string ToString()
    {
        return string.Format("(q:{0}, r:{1}, s:{2}", q, r, s);
    }

    public static HexPoint zero
    {
        get
        {
            return new HexPoint(0,0);
        }
    }

    #region Equality Methods

    public bool Equals(HexPoint other)
    {
        return q == other.q && r == other.r && s == other.s;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is HexPoint && Equals((HexPoint) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = q;
            hashCode = (hashCode*397) ^ r;
            hashCode = (hashCode*397) ^ s;
            return hashCode;
        }
    }

    #endregion

    #region Constructors
    /// <summary>
    /// Cube Coordinate Constructor
    /// </summary>
    /// <param name="x">Q value</param>
    /// <param name="z">R value</param>
    /// <param name="y">-Q-R value</param>
    public HexPoint(int x, int z, int y)
    {
        q = x;
        r = z;
        s = y;

        if(!Verify())
            Debug.LogError(String.Format("HexPoint Creation Attempt Failed: Q:{0} R:{1} S:{2} != 0", q, r, s));
    }

    /// <summary>
    /// Axial Coordinate Constructor
    /// </summary>
    /// <param name="x">Q value</param>
    /// <param name="z">R value</param>
    public HexPoint(int x, int z)
    {
        q = x;
        r = z;
        s = -x - z;

        if(!Verify())
            Debug.LogError(String.Format("HexPoint Creation Attempt Failed: Q:{0} R:{1} S:{2} != 0", q, r, s));
    }

    #endregion

    #region Variables

    public readonly int q; /*x*/
    public readonly int r; /*z*/
    public readonly int s; /*y*/

    public static HexPoint[] Directions = new HexPoint[]
    {
        new HexPoint(1, -1, 0), //East
        new HexPoint(1, 0, -1), //Northeast
        new HexPoint(0, +1, -1), //Northwest
        new HexPoint(-1, 1, 0), //West
        new HexPoint(-1, 0, 1), //Southwest
        new HexPoint(0, -1, 1), //Southeast      
    };

    /* -----------CONSTANTS----------*/
    public static readonly float SQRT3 = Mathf.Sqrt(3);
    public const float ONE_THIRD = 1f/3f;
    public const float TWO_THIRDS = 2f/3f;
    /*-------------------------------*/

    #endregion

    #region Navigation

    //Returns 0,0 if not a direction.
    public static HexPoint GetDirection(int direction /*0 to 5, starts to east*/)
    {
        if (direction >= 0 && direction < Directions.Length)
            return Directions[direction];
        return new HexPoint(0,0);
    }

    public static HexPoint GetNeighbor(HexPoint hex, int direction)
    {
        return hex + GetDirection(direction);
    }

    public static HexPoint[] GetAllNeighbors(HexPoint hex)
    {
        List<HexPoint> temp = new List<HexPoint>();
        foreach (var d in Directions)
        {
            temp.Add(hex + d);
        }
        return temp.ToArray();
    }

    #endregion

    #region Instance Methods

    /// <summary>
    /// Converts HexPoint from grid space to world space.
    /// </summary>
    /// <param name="size">Radius of the Hexagon</param>
    /// <param name="yPos">The extruded height of the tile</param>
    /// <returns>Vector3 World Position</returns>
    public static implicit operator Vector3(HexPoint hex)
    {
        var x = Map.Instance.size * SQRT3 * ( hex.q + .5f * hex.r );
        var z = Map.Instance.size * 1.5f * hex.r;

        return new Vector3(x, Map.Instance.MapGrid[hex].YPos, z);
    }

    /// <summary>
    /// Converts a Vector3 from WorldSpace to a HexPoint. Does not check for a tile to exist at the point.
    /// Only returns the HexPoint where a tile WOULD be if there was one.
    /// </summary>
    /// <param name="size">Radius of the Hexagon</param>
    /// <param name="worldPos">Vector3 of the world location</param>
    /// <returns>HexPoint of the tile this space would be located in</returns>
    public static implicit  operator HexPoint(Vector3 worldPos)
    {
        float qx = (worldPos.x*ONE_THIRD*SQRT3 - worldPos.z*ONE_THIRD)/Map.Instance.size;
        float rx = (worldPos.z*TWO_THIRDS)/Map.Instance.size;

        return RoundToHexPoint(new FractionalHex(qx, rx));
    }

    #endregion

    #region Operators

    public static bool operator ==(HexPoint a, HexPoint b)
    {
        return a.q == b.q && a.r == b.r && a.s == b.s;
    }

    public static bool operator !=(HexPoint a, HexPoint b)
    {
        return !(a == b);
    }

    public static HexPoint operator +(HexPoint a, HexPoint b)
    {
        return new HexPoint(a.q + b.q, a.r + b.r, a.s + b.s);
    }
    public static HexPoint operator -(HexPoint a, HexPoint b)
    {
        return new HexPoint(a.q - b.q, a.r - b.r, a.s - b.s);
    }

    #endregion

    #region Arithmetic Methods

    /// <summary>
    /// Will multiply the entire HexPoint by the scaleFactor 
    /// </summary>
    /// <param name="a">HexPoint to scale</param>
    /// <param name="scaleFactor">Factor to scale by</param>
    /// <returns>HexPoint adjusted according to scaleFactor</returns>
    public static HexPoint Scale(HexPoint a, int scaleFactor)
    {
        return new HexPoint(a.q * scaleFactor, a.r * scaleFactor, a.s * scaleFactor);
    }

    /// <summary>
    /// Returns the length of a HexPoint Vector. Useful if you already know the difference between two HexPoints.
    /// </summary>
    /// <param name="hex">HexPoint that will be measured like a Vector</param>
    /// <returns>Integer equal to the distance of the Vector</returns>
    public static int Length(HexPoint hex)
    {
        return (int)((Mathf.Abs(hex.q) + Mathf.Abs(hex.r) + Mathf.Abs(hex.s))/2);
    }

    /// <summary>
    /// Returns the Length of the HexPoint vector between points A and B.
    /// </summary>
    /// <param name="a">Starting HexPoint</param>
    /// <param name="b">Ending HexPoint</param>
    /// <returns>Integer equal to the length of a - b</returns>
    public static int Distance(HexPoint a, HexPoint b)
    {
        return Length(a - b);
    }


    /// <summary>
    /// Rounds a Fractional Hex to a HexPoint. Useful for converting world coordinates to grid coordinates.
    /// Does not guarantee a tile will be located at the HexPoint returned by this method.
    /// </summary>
    /// <param name="hex">FractionalHex that will be rounded to a HexPoint</param>
    /// <returns>HexPoint of the location a tile should be at.</returns>
    public static HexPoint RoundToHexPoint(FractionalHex hex)
    {
        int qx = Mathf.RoundToInt(hex.q);
        int rx = Mathf.RoundToInt(hex.r);
        int sx = Mathf.RoundToInt(hex.s);

        float qDiff = Mathf.Abs(qx - hex.q);
        float rDiff = Mathf.Abs(rx - hex.r);
        float sDiff = Mathf.Abs(sx - hex.s);

        if (qDiff > rDiff && qDiff > sDiff)
            qx = -rx - sx;
        else if (rDiff > sDiff)
            rx = -qx - sx;
        else
            sx = -qx - rx;

        return new HexPoint(qx,rx,sx);
    }
    #endregion 

    #region Private Helper Methods

    private bool Verify()
    {
        return q + r + s == 0;
    }
    #endregion
}