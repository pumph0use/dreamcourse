﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Map))]
public class MapInspector : Editor
{
    private Map map;
    private bool showSceneMenu;

    void OnSceneGUI()
    {
        map = target as Map;

        //Begin custom handles
        Handles.BeginGUI();

        //Begin GUI area in scene view
        GUILayout.BeginArea(new Rect(Screen.width - 100, Screen.height - 80, 90, 50));
        if (GUILayout.Button("Reset Map"))
        {
            Undo.RecordObject(map, "Reset Map");
            map.MapReset();
            EditorUtility.SetDirty(map);
        }
        
        //end GUI area in scene view
        GUILayout.EndArea();
   
        //End custom handles
        Handles.EndGUI();

    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
}
