﻿public struct FractionalHex
{
    #region Variables

    public float q;
    public float r;
    public float s;

    #endregion

    #region Constructors

    public FractionalHex(float q, float r)
    {
        this.q = q;
        this.r = r;
        s = -q - r;
    }

    public FractionalHex(float q, float r, float s)
    {
        this.q = q;
        this.r = r;
        this.s = s;
    }

    #endregion
}