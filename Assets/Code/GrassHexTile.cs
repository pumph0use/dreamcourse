﻿using UnityEngine;

public class GrassHexTile : IHexTile
{
    private float yPos;

    public GrassHexTile(float yPos)
    {
        this.yPos = yPos;
    }

    public float YPos
    {
        get { return yPos; }
        set { yPos = value; }
    }

    public void BuildTile(HexMeshData meshData, HexPoint center)
    {

        HexBuilder.GenerateHexTop(center, YPos, meshData);
        HexBuilder.GenerateWall(center, 0, meshData);
        HexBuilder.GenerateWall(center, 1, meshData);
        HexBuilder.GenerateWall(center, 2, meshData);
        HexBuilder.GenerateWall(center, 3, meshData);
        HexBuilder.GenerateWall(center, 4, meshData);
        HexBuilder.GenerateWall(center, 5, meshData);
    }
}